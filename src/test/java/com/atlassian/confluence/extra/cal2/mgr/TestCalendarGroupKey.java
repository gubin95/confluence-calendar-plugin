package com.atlassian.confluence.extra.cal2.mgr;

import com.atlassian.confluence.extra.cal2.mgr.CalendarGroupKey;

import junit.framework.TestCase;

public class TestCalendarGroupKey extends TestCase 
{
	private static final String TEST_SPACE_KEY = "test";
	private static final String TEST_PAGE_KEY = "Home";
	private static final String TEST_GROUP_KEY = "group";

	private static final String EXPECTED_GROUP_KEY = "test:Home:group";

	public TestCalendarGroupKey(String name) 
	{
		super(name);
	}

	// test value construction
	public void testKeyValueConstruction() 
	{
		CalendarGroupKey groupKey = new CalendarGroupKey(TEST_SPACE_KEY, TEST_PAGE_KEY, TEST_GROUP_KEY);
		assertEquals(EXPECTED_GROUP_KEY, groupKey.getValue());
	}
	
	
	// test equality
	public void testEquality()
	{
		CalendarGroupKey key1 = new CalendarGroupKey(TEST_SPACE_KEY, TEST_PAGE_KEY, TEST_GROUP_KEY);
		CalendarGroupKey key2 = new CalendarGroupKey(TEST_SPACE_KEY, TEST_PAGE_KEY, TEST_GROUP_KEY);
		CalendarGroupKey key3 = new CalendarGroupKey(TEST_SPACE_KEY, TEST_PAGE_KEY + "extra", TEST_GROUP_KEY);

		assertEquals(key1, key2);
		assertTrue( !key1.equals(key3));
	}
	
	// test hashcode equality
	public void testHashCode()
	{
		CalendarGroupKey key1 = new CalendarGroupKey(TEST_SPACE_KEY, TEST_PAGE_KEY, TEST_GROUP_KEY);
		CalendarGroupKey key2 = new CalendarGroupKey(TEST_SPACE_KEY, TEST_PAGE_KEY, TEST_GROUP_KEY);
		CalendarGroupKey key3 = new CalendarGroupKey(TEST_SPACE_KEY, TEST_PAGE_KEY + "extra", TEST_GROUP_KEY);

		assertEquals(key1.hashCode(), key2.hashCode());
		assertTrue( key1.hashCode() != key3.hashCode());
	}
}
