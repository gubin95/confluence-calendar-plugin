package com.atlassian.confluence.extra.cal2.mgr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

import javax.xml.rpc.holders.CalendarHolder;

import junit.framework.TestCase;

public class TestCalendarManager extends TestCase
{
    private static final String TEST_SPACE_KEY = "test";
    private static final String TEST_SPACE2_KEY = "test2";
    private static final String TEST_PAGE_KEY = "Home";
    private static final String TEST_GROUP_KEY_PREFIX = "group";
    
    private CalendarManager manager;

    private int idNumber;
    
    public TestCalendarManager(String name)
    {
        super(name);
    }

    @Override
    protected void setUp() throws Exception
    {
        super.setUp();

        manager = new CalendarManager();
        idNumber = 1;
    }


    // add calendar/group
    public void testAddCalendars()
    {
//        Collection<CalendarHolder> expected = new HashSet<CalendarHolder>();
//
//        CalendarHolder holder = new DefaultCalendarHolder();
//        expected.add(holder);
//        manager.addCalendarHolder(holder);
//
//        holder = new DefaultCalendarHolder();
//        expected.add(holder);
//        manager.addCalendarHolder(holder);
//
//        holder = new DefaultCalendarHolder();
//        expected.add(holder);
//        manager.addCalendarHolder(holder);
//
//        assertTrue("Add calendars", sameContents( expected, manager.getCalendarHolders() ) );
    }

    private boolean sameContents(Collection expected, Collection result) {
		// TODO Auto-generated method stub
		return ( expected.containsAll(result) && result.containsAll(expected) );
	}

	public void testAddGroups()
    {
        List<CalendarGroup> expected = new ArrayList<CalendarGroup>();

        CalendarGroup group = new CalendarGroup(makeNewKey());
        expected.add(group);
        manager.addCalendarGroup(group);

        group = new CalendarGroup(makeNewKey());
        expected.add(group);
        manager.addCalendarGroup(group);

        group = new CalendarGroup(makeNewKey());
        expected.add(group);
        manager.addCalendarGroup(group);

        assertTrue("Add groups", sameContents( expected, manager.getCalendarGroups()) );
    }

    // remove calendar/group
    public void testRemoveCalendars()
    {
//        List<CalendarHolder> expected = new ArrayList<CalendarHolder>();
//
//        CalendarHolder holder1 = new DefaultCalendarHolder();
//        manager.addCalendarHolder(holder1);
//
//        CalendarHolder holder2 = new DefaultCalendarHolder();
//        expected.add(holder2);
//        manager.addCalendarHolder(holder2);
//
//        holder2 = new DefaultCalendarHolder();
//        expected.add(holder2);
//        manager.addCalendarHolder(holder2);
//
//        manager.removeCalendarHolder(holder1.getId());
//
//        assertTrue("remove calendars", sameContents( expected, manager.getCalendarHolders() ) );
    }

    public void testRemoveGroups()
    {
        List<CalendarGroup> expected = new ArrayList<CalendarGroup>();

        CalendarGroup group1 = new CalendarGroup(makeNewKey());
        manager.addCalendarGroup(group1);

        CalendarGroup group2 = new CalendarGroup(makeNewKey());
        expected.add(group2);
        manager.addCalendarGroup(group2);

        group2 = new CalendarGroup(makeNewKey());
        expected.add(group2);
        manager.addCalendarGroup(group2);

        manager.removeCalendarGroup(group1.getHashKey());

        assertTrue("remove groups", sameContents( expected, manager.getCalendarGroups()) );
    }

    // get calendar/group
    public void testGetCalendar()
    {
//        CalendarHolder holder1 = new DefaultCalendarHolder();
//        manager.addCalendarHolder(holder1);
//
//        CalendarHolder holder2 = new DefaultCalendarHolder();
//        manager.addCalendarHolder(holder2);
//
//        assertSame("Calendar holder 1", holder1, manager.getCalenderHolder(holder1.getId()));
//        assertSame("Calendar holder 2", holder2, manager.getCalenderHolder(holder2.getId()));
    }

    public void testGetGroupWithoutCreate()
    {
    	CalendarGroupKey key1 = makeNewKey();
        CalendarGroup group1 = new CalendarGroup(key1);
        manager.addCalendarGroup(group1);

    	CalendarGroupKey key2 = makeNewKey();
        CalendarGroup group2 = new CalendarGroup(key2);
        manager.addCalendarGroup(group2);
        
    	CalendarGroupKey key3 = makeNewKey();
        

        assertSame("Calendar group 1", group1, manager.getCalendarGroup(key1, false));
        assertSame("Calendar group 2", group2, manager.getCalendarGroup(key2, true));
        assertNull("Non-existant group", manager.getCalendarGroup(key3, false));
    }

    public void testGetGroupWithCreate()
    {
    	CalendarGroupKey key1 = makeNewKey();
        CalendarGroup group1 = new CalendarGroup(key1);
        manager.addCalendarGroup(group1);

    	CalendarGroupKey key2 = makeNewKey();
        CalendarGroup group2 = new CalendarGroup(key2);
        manager.addCalendarGroup(group2);
        
    	CalendarGroupKey key3 = makeNewKey();
        

        assertSame("Calendar group 1", group1, manager.getCalendarGroup(key1, false));
        assertSame("Calendar group 2", group2, manager.getCalendarGroup(key2, true));
        
        CalendarGroup group3 = manager.getCalendarGroup(key3, true);
        assertNotNull("Created group", group3);

    }

    // get calendars from group
    public void testGetCalendarsForGroup()
    {
//        // create/add calendar holders
//        List<CalendarHolder> expected1 = new ArrayList<CalendarHolder>();
//        List<CalendarHolder> expected2 = new ArrayList<CalendarHolder>();
//    	
//        CalendarHolder holder1 = new DefaultCalendarHolder();
//        manager.addCalendarHolder(holder1);
//        CalendarHolder holder2 = new DefaultCalendarHolder();
//        manager.addCalendarHolder(holder2);
//        CalendarHolder holder3 = new DefaultCalendarHolder();
//        manager.addCalendarHolder(holder3);
//        
//        expected1.add(holder1);
//        expected1.add(holder2);
//        expected2.add(holder3);
//        expected2.add(holder1);
//        
//        // create group 
//        CalendarGroupKey key1 = makeNewKey();
//        CalendarGroup group1 = new CalendarGroup(key1);
//        group1.add(holder1.getId());
//        group1.add(holder2.getId());
//        manager.addCalendarGroup(group1);
//
//        CalendarGroupKey key2 = makeNewKey();
//        CalendarGroup group2 = new CalendarGroup(key2);
//        group2.add(holder3.getId());
//        group2.add(holder1.getId());
//        manager.addCalendarGroup(group2);
//        
//		// get calendars
//		List<CalendarHolder> g1 = manager.getHoldersForGroup(key1);
//		List<CalendarHolder> g2 = manager.getHoldersForGroup(key2);
//		
//		// verify
//		assertEquals("Holders from group 1", expected1, g1);
//		assertEquals("Holders from group 2", expected2, g2);
    }

    // get groups for space
    public void testGetGroupsForSpace()
    {
        List<CalendarGroup> expected1 = new ArrayList<CalendarGroup>();
        List<CalendarGroup> expected2 = new ArrayList<CalendarGroup>();

        // create groups in multiple spaces
        CalendarGroup group = new CalendarGroup(makeNewKey(TEST_SPACE_KEY));
        expected1.add(group);
        manager.addCalendarGroup(group);
        
        group = new CalendarGroup(makeNewKey(TEST_SPACE_KEY));
        expected1.add(group);
        manager.addCalendarGroup(group);
        
        group = new CalendarGroup(makeNewKey(TEST_SPACE2_KEY));
        expected2.add(group);
        manager.addCalendarGroup(group);
        
        group = new CalendarGroup(makeNewKey(TEST_SPACE2_KEY));
        expected2.add(group);
        manager.addCalendarGroup(group);
        
        // verify get groups for space 1
        assertTrue( "Groups for space 1", sameContents(expected1, manager.getGroupsForSpace(TEST_SPACE_KEY)));

        // verify get groups for space 2
        assertTrue( "Groups for space 2", sameContents(expected2, manager.getGroupsForSpace(TEST_SPACE2_KEY)));
    }
    
    private CalendarGroupKey makeNewKey()
    {
    	return makeNewKey(TEST_SPACE_KEY);
    }
    
    private CalendarGroupKey makeNewKey(String spaceKey)
    {
        return new CalendarGroupKey( spaceKey, TEST_PAGE_KEY, TEST_GROUP_KEY_PREFIX + idNumber++ );
    }
}
