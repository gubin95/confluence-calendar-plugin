/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.cal2.model;

import org.joda.time.DateTime;

import java.util.Comparator;

/**
 * This class compares two IEvent objects based on their start date,
 * all-dayness, end date and summary.
 * 
 */
public class EventComparator implements Comparator {
    public int compare( Object o1, Object o2 ) {
        IEvent evt1 = ( IEvent ) o1;
        IEvent evt2 = ( IEvent ) o2;

        int comp = 0;

        DateTime startDate1 = evt1.getStartDate().toDateTime( evt1.getTimeZone() );
        DateTime startDate2 = evt2.getStartDate().toDateTime( evt2.getTimeZone() );

        if ( startDate1 == null && startDate2 == null )
            comp = 0;
        else if ( startDate1 != null )
            comp = startDate1.compareTo( startDate2 );
        else
            comp = -1;

        if ( comp == 0 ) // compare the all-dayness
        {
            if ( evt1.isAllDay() && !evt2.isAllDay() )
                return -1;
            else if ( evt2.isAllDay() && !evt1.isAllDay() )
                return 1;
        }

        if ( comp == 0 ) // compare the end date
        {
            if ( evt1.getEndDate() != null && evt2.getEndDate() != null )
                comp = evt1.getEndDate().compareTo( evt2.getEndDate() );
            else if ( evt2.getEndDate() != null )
                comp = 1;
        }

        if ( comp == 0 ) // compare the summary
        {
            if ( evt1.getSummary() != null )
                comp = evt1.getSummary().compareTo( evt2.getSummary() );
            else if ( evt2.getSummary() != null )
                comp = 1;
        }

        return comp;
    }
}
