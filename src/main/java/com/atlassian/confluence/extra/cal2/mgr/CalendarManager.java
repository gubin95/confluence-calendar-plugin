package com.atlassian.confluence.extra.cal2.mgr;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.omg.PortableInterceptor.SUCCESSFUL;

import net.fortuna.ical4j.model.property.Uid;

import com.atlassian.bandana.BandanaManager;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.extra.cal2.model.ICalCalendar;
import com.atlassian.confluence.extra.cal2.model.ICalendar;
import com.atlassian.confluence.setup.bandana.ConfluenceBandanaContext;
import com.atlassian.confluence.user.UserAccessor;

/**
 * Provides shared access to CalendarGroups and Calendar Holders
 * 
 * @author ndwyer
 * 
 */
public class CalendarManager
{
	private static final String CONFIGURATION_KEY = "confluence.extra.calendar:configuration";
;

    private static CalendarManager instance;

	private UserAccessor userAccessor;
	private BandanaManager bandanaManager;

	// map from CalendarID to CalendarHolder
	private Map<String, ICalendar> idToHolderMap = new HashMap<String, ICalendar>();

	// map from CalendarGroupKey to CalendarGroup
	private Map<CalendarGroupKey, CalendarGroup> keyToGroupMap = new HashMap<CalendarGroupKey, CalendarGroup>();

	public CalendarManager()
	{
		// if( instance != null )
		// throw new Exception...
		instance = this;
	}

	// Only usable within the package
	static CalendarManager getInstance()
	{
		return instance;
	}

	// add calendar/group
	public void addCalendarHolder(ICalendar holder)
	{
		idToHolderMap.put(holder.getId(), holder);
	}

	public void addCalendarGroup(CalendarGroup group)
	{
		keyToGroupMap.put(group.getHashKey(), group);
	}

	// remove calendar/group
	public void removeCalendarHolder(String holderId)
	{
		// Remove this holder from all the groups
		for (CalendarGroup group : getCalendarGroups())
		{
			group.remove(holderId);
		}

		idToHolderMap.remove(holderId);
	}

	public void removeCalendarGroup(CalendarGroupKey groupKey)
	{
		keyToGroupMap.remove(groupKey);
	}

	// Accessors for collection data members
	public Collection<CalendarGroup> getCalendarGroups()
	{
		return keyToGroupMap.values();
	}

	public Collection<ICalendar> getCalendarHolders()
	{
		return idToHolderMap.values();
	}

	public ICalendar getCalenderHolder(String holderId)
	{
		return idToHolderMap.get(holderId);
	}

	public CalendarGroup getCalendarGroup(CalendarGroupKey groupKey)
	{
		return getCalendarGroup(groupKey, false);
	}

	public CalendarGroup getCalendarGroup(CalendarGroupKey groupKey, boolean createIfNeeded)
	{
		CalendarGroup result = keyToGroupMap.get(groupKey);
		if ((result == null) && createIfNeeded)
		{
			result = new CalendarGroup(groupKey);
			addCalendarGroup(result);

			// TODO TEMP
			ICalCalendar cal = new ICalCalendar();
			cal.setColor("blue");
			cal.setName("Test calendar");
			cal.setDescription("A test calendar");
			addCalendarHolder(cal);
			result.add(cal.getId());
		}

		return result;
	}

	// get calendars from group
	public List<ICalendar> getHoldersForGroup(CalendarGroupKey groupKey)
	{
		CalendarGroup group = getCalendarGroup(groupKey);
		// if( group == null)
		// throw new CalendarException( "Group not found");

		List<ICalendar> result = new ArrayList<ICalendar>();
		for (String id : group.getMembers())
		{
			ICalendar holder = getCalenderHolder(id);
			// if( holder == null )
			// throw new CalendarException( "Holder not found");

			// TODO What's the appropriate error handling for this?
			if (holder != null)
				result.add(holder);
		}

		return result;
	}

	// get groups for space
	public List<CalendarGroup> getGroupsForSpace(String spaceKey)
	{
		List<CalendarGroup> result = new ArrayList<CalendarGroup>();
		;

		for (CalendarGroup group : getCalendarGroups())
		{
			if (group.getHashKey().getSpaceKey().equals(spaceKey))
				result.add(group);
		}

		return result;
	}

	
    public Uid getNewEventId()
    {
        UUID uuid = UUID.randomUUID();
        return new Uid(uuid.toString().toUpperCase());
    }


	// serialization
    public void saveCalendar(ContentEntityObject content, String calendarId, CalendarGroup calendarGroup) throws CalendarException
    {
    	// TODO Auto-generated method stub
    }
    
    private CalendarProperties calendarProperties;
    
    public CalendarProperties getConfiguration()
    {
    	if( calendarProperties == null )
    	{
    		calendarProperties = (CalendarProperties) bandanaManager.getValue(new ConfluenceBandanaContext(), CONFIGURATION_KEY);
    	
        	if( calendarProperties == null)
        	    calendarProperties = new CalendarProperties();
    	}
    	
    	return calendarProperties;
    }
    
    public void saveConfiguration()
    {
        if( calendarProperties != null )
            bandanaManager.setValue(new ConfluenceBandanaContext(), CONFIGURATION_KEY, calendarProperties);
    }

    
    // Autowired components
	
	public UserAccessor getUserAccessor()
	{
		return userAccessor;
	}

	public void setUserAccessor(UserAccessor userAccessor)
	{
		this.userAccessor = userAccessor;
	}

    public void setBandanaManager(BandanaManager bandanaManager)
    {
        this.bandanaManager = bandanaManager;
    }
    
}
