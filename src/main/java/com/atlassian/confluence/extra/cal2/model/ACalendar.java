/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.cal2.model;

import java.util.List;

import com.opensymphony.util.GUID;

/**
 * An abstract base class for ICalendar implementation to use if they wish.
 */
public abstract class ACalendar implements ICalendar {
    private String id;

    private String name;

    private String description;

    private String color;

    private transient List errors;

    private String suppressSubscribeLink;

    protected ACalendar() {
        id = GUID.generateGUID();
    }

    public String getId() {
        return id;
    }

    protected void setId( String id ) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName( String name ) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription( String description ) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor( String color ) {
        this.color = color;
    }

    public void setSuppressSubscribeLink(String suppressSubscribeLink) {
        this.suppressSubscribeLink = suppressSubscribeLink;
    }

    public String getSuppressSubscribeLink() {
        return suppressSubscribeLink;
    }

    /**
     * Returns a list of error message Strings, or null if none exist.
     * 
     * @return The list of error Strings.
     */
    public List getErrors() {
        return errors;
    }
    
    /**
     * Clears any errors from the calendar.
     */
    protected void clearErrors() {
        errors = null;
    }

    /**
     * Adds an error message to the calendar.
     * 
     * @param message
     */
    protected void addError( String message ) {
        if ( errors == null ) {
            errors = new java.util.LinkedList();
        }
        errors.add( message );
    }

    /**
     * Checks if the current object is read-only and throws an
     * UnsupportedOperationException if it is.
     * <p>
     * The intention of this method is to make it easy for public 'setXXX'
     * methods on subclasses to test read-only status.
     * 
     * @throws UnsupportedOperationException
     *             if the object is read-only.
     * @see #isReadOnly()
     */
    protected void checkReadOnly() {
        if ( isReadOnly() )
            throw new UnsupportedOperationException( "This value is read-only." );
    }

}
