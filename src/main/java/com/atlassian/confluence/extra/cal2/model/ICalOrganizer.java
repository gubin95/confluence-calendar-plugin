/*
 * Copyright (c) 2006, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package com.atlassian.confluence.extra.cal2.model;

import java.net.URI;
import java.net.URISyntaxException;

import net.fortuna.ical4j.model.Parameter;
import net.fortuna.ical4j.model.parameter.Cn;
import net.fortuna.ical4j.model.parameter.XParameter;
import net.fortuna.ical4j.model.property.Organizer;

/**
 * Represents an organiser of the event.
 */
public class ICalOrganizer extends ICalObject {
    private static final String USERNAME = Parameter.EXPERIMENTAL_PREFIX + "CONF-USERNAME";

    private EventDetails details;

    private Organizer organizer;

    /**
     * 
     */
    public ICalOrganizer() {
        this( null, null );
    }

    public ICalOrganizer( EventDetails details, Organizer organizer ) {
        this.details = details;

        if ( organizer == null ) {
            organizer = new Organizer();

            if ( details != null )
                details.addProperty( organizer );
        }

        this.organizer = organizer;
    }

    public String getName() {
        Cn cn = ( Cn ) organizer.getParameters().getParameter( Parameter.CN );
        if ( cn != null )
            return cn.getValue();
        return null;
    }

    public void setName( String name ) {
        checkReadOnly();

        Cn cn = ( Cn ) organizer.getParameters().getParameter( Parameter.CN );

        if ( cn != null )
            organizer.getParameters().remove( cn );

        cn = new Cn( name );
        organizer.getParameters().add( cn );
    }

    public String getUsername() {
        XParameter un = ( XParameter ) organizer.getParameters().getParameter( USERNAME );
        if ( un != null )
            return un.getValue();
        return null;
    }

    public void setUsername( String username ) {
        checkReadOnly();

        XParameter un = ( XParameter ) organizer.getParameters().getParameter( USERNAME );

        if ( un != null )
            organizer.getParameters().remove( un );

        un = new XParameter( USERNAME, username );
        organizer.getParameters().add( un );
    }

    public URI getAddress() {
        return organizer.getCalAddress();
    }

    public void setAddress( URI address ) {
        checkReadOnly();

        organizer.setCalAddress( address );
    }

    public String getEmail() {
        URI address = getAddress();
        if ( address != null && address.getScheme().equals( "mailto" ) )
            return address.getPath();

        String value = organizer.getValue();
        if ( value != null && value.startsWith( "mailto:" ) )
            return value.substring( 7 );
        else
            return null;
    }

    /**
     * Sets the email.
     * 
     * @param email
     *            The email address
     * @throws URISyntaxException
     *             If there is an error in the address
     * @throws IllegalArgumentException
     *             if the email is null or blank.
     */
    public void setEmail( String email ) throws URISyntaxException {
        checkReadOnly();

        if ( email != null && email.trim().length() > 0 )
            organizer.setCalAddress( new URI( "mailto:" + email ) );
        else
            organizer.setCalAddress( null );
    }

    public Organizer getOrganizerProperty() {
        return organizer;
    }

    /**
     * Updates the event.
     * 
     * @param details
     *            The event details object
     */
    void setDetails( EventDetails details ) {
        this.details = details;
    }

    public boolean isReadOnly() {
        return details != null && details.isReadOnly();
    }
}
