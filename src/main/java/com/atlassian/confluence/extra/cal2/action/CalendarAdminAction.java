package com.atlassian.confluence.extra.cal2.action;

import net.fortuna.ical4j.util.CompatibilityHints;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.extra.cal2.mgr.CalendarManager;
import com.atlassian.confluence.extra.cal2.mgr.CalendarProperties;

/**
 * View and update the license key for this Confluence installation
 */
public class CalendarAdminAction extends ConfluenceActionSupport {

	private CalendarManager calendarManager;
	
    /**
     * Called to start the administration page
     * 
     * @return
     * @throws Exception
     */
    public String doUpdate() throws Exception {
        return SUCCESS;
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isOutlookCompatible() {
        return getValue(CalendarProperties.OUTLOOK_COMPATIBLE_KEY);
    }


	/**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotOutlookCompatible() {
    	return setValue(CalendarProperties.OUTLOOK_COMPATIBLE_KEY, false);
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsOutlookCompatible() {
    	return setValue(CalendarProperties.OUTLOOK_COMPATIBLE_KEY, true);
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isNotesCompatible() {
        return getValue(CalendarProperties.NOTES_COMPATIBLE_KEY);

    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotNotesCompatible() {
    	return setValue(CalendarProperties.NOTES_COMPATIBLE_KEY, false);
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsNotesCompatible() {
    	return setValue(CalendarProperties.NOTES_COMPATIBLE_KEY, true);
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isRelaxedUnfolding() {
        return getValue(CalendarProperties.RELAXED_UNFOLDING_KEY);

    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotRelaxedUnfolding() {
    	return setValue(CalendarProperties.RELAXED_UNFOLDING_KEY, false);
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsRelaxedUnfolding() {
    	return setValue(CalendarProperties.RELAXED_UNFOLDING_KEY, true);
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isRelaxedParsing() {
        return getValue(CalendarProperties.RELAXED_PARSING_KEY);

    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotRelaxedParsing() {
    	return setValue(CalendarProperties.RELAXED_PARSING_KEY, false);
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsRelaxedParsing() {
    	return setValue(CalendarProperties.RELAXED_PARSING_KEY, true);
    }

    /**
     * Determine whether or not he compatibility flag is set.
     * 
     * @return
     */
    public boolean isRelaxedValidation() {
        return getValue(CalendarProperties.RELAXED_VALIDATION_KEY);

    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setNotRelaxedValidation() {
    	return setValue(CalendarProperties.RELAXED_VALIDATION_KEY, false);
    }

    /**
     * Modify the compatibility setting.
     * 
     * @return
     */
    public String setIsRelaxedValidation() {
    	return setValue(CalendarProperties.RELAXED_VALIDATION_KEY, true);
    }

    
    private String setValue(String key, boolean value) 
    {
    	// TODO  Override isPermitted to check permissions
    	if( !isPermitted() )
    		return ERROR;

    	CompatibilityHints.setHintEnabled( key, value );
    	getConfiguration().setProperty(key, Boolean.toString(value));
    	saveConfiguration();
    	
    	return SUCCESS;
    }
    
    private boolean getValue( String key )
    {
    	return Boolean.parseBoolean(getConfiguration().getProperty(key, Boolean.FALSE.toString()));
    }
    

    private CalendarProperties getConfiguration()
	{
		return calendarManager.getConfiguration();
	}
    
    private void saveConfiguration()
    {
    	calendarManager.saveConfiguration();
    }
    
	public void setCalendarManager(CalendarManager calendarManager)
	{
		this.calendarManager = calendarManager;
	}

    
}
