/*
 * Copyright (c) 2007, Atlassian Software Systems Pty Ltd
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of "Atlassian" nor the names of its contributors
 *       may be used to endorse or promote products derived from this software
 *       without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

package com.atlassian.confluence.extra.cal2.action;

import org.randombits.source.StringSource;

import com.atlassian.confluence.extra.cal2.mgr.CalendarException;
import com.atlassian.confluence.extra.cal2.model.ICalCalendar;

/**
 * TODO: Document this class.
 * 
 * @author David Peterson
 */
public class ImportICalCalendarAction extends CalendarHolderAction {
    public ImportICalCalendarAction() {
    	setRequireEditPermission();
    }

    /**
     * Import a calendar and convert it to a local calendar.
     * 
     * @return
     */
    public String doImport() {
        ICalCalendar calendar = ( ICalCalendar ) getSubCalendar();

        if ( !( calendar.getSource() instanceof StringSource ) ) {
            try {
                // Load the calendar
                calendar.getCalendar();
                // New source type
                calendar.setSource( new StringSource() );

                // Save it.
                return saveCalendar();
            } catch ( CalendarException e ) {
                e.printStackTrace();
                addActionError( getText( "calendar.error.ical.calendarException", e.getMessage() ) );
                return ERROR;
            }
        } else {
            addActionError( getText( "calendar.error.ical.source.alreadyImported" ) );
            return ERROR;
        }
    }
}
