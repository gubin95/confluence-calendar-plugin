package com.atlassian.confluence.extra.cal2.mgr;


public class CalendarException extends Exception
{
	public CalendarException(String msg)
	{
		super( msg );
	}

    public CalendarException(Exception e)
    {
        super(e);
    }
}
